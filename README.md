# Checkers
This is a two player command-line checkers game implemented in PHP. Includes a
nice cow that will help you with incorrect inputs and comments on your moves.

## Dependencies
- PHP.
- That's all.

## Usage
```
php main.php
```

## Rules
This implementation is based on the rules of [German
draughts](https://en.wikipedia.org/wiki/Draughts). This roughly means:
- All movement is diagonal.
- You have to jump when possible.
- Normal pieces can only move forwards.
- But they can jump both forwards and backwards.
- One piece can chain multiple jumps.
- If a man ends his turn on the opposite side, it becomes a king.
- Kings can move as far as they want.
- Kings must land directly after the piece they capture.
