<?php

const ALPHABET = "ABCDEFGHIJKLMOPQRSTUVWXYZ";


abstract class Team {
    const Black = 0;
    const White = 1;
}


abstract class PieceType {
    const Normal = 0;
    const King = 1;
}


const COW = "        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
";

// Matches the basic functionality of the cowsay program.
function cowsay($text) {
    $text = explode(PHP_EOL, $text);
    $max = 0;
    foreach ($text as $line) {
        if (strlen($line) > $max) {
            $max = strlen($line);
        }
    }
    $lines = [];
    array_push($lines, " " . str_repeat("_", $max+2));
    if (sizeof($text) == 1) {
        array_push($lines, "< " . $text[0] . " >");
    } else {
        foreach ($text as $key=>$line) {
            if ($key == 0) {
                $open = '/';
                $close = '\\';
            } else if ($key == sizeof($text)-1) {
                $open = '\\';
                $close = '/';
            } else {
                $open = '|';
                $close = '|';
            }
            array_push($lines, $open . " " . str_pad($line, $max) . " " . $close);
        }
    }
    array_push($lines, " " . str_repeat("-", $max+2));
    array_push($lines, ...explode("\n", COW));
    return $lines;
}


function draw_cow_line($cowlines, $linenum) {
    $shift = 19 - sizeof($cowlines);
    if ($linenum >= $shift) {
        echo str_repeat(" ", 3) . $cowlines[$linenum-$shift];
    }
}


class Pos {
    public $x = 0;
    public $y = 0;

    function __construct($x, $y) {
        $this->x = $x;
        $this->y = $y;
    }

    function add($other) {
        return new Pos($this->x + $other->x, $this->y + $other->y);
    }

    static function fromCoordinates($coordinates) {
        $x = intval($coordinates[1])-1;
        $y = strpos(ALPHABET, strtoupper($coordinates[0]));
        return new Pos($x, $y);
    }

    function toCoordinates() {
        $x = strval($this->x + 1);
        $y = ALPHABET[$this->y];
        return $y . $x;
    }

    function inBounds($size) {
        return $this->x >= 0 && $this->x < $size && $this->y >= 0 && $this->y < $size;
    }
}


class Piece {
    public $team = 0;
    public $type = PieceType::Normal;

    function __construct($team) {
        $this->team = $team;
        $this->type = PieceType::Normal;
    }

    function __toString() {
        if ($this->team == Team::White) {
            if ($this->type == PieceType::Normal) {
                return "●";
            } else {
                return "◉";
            }
        } else {
            if ($this->type == PieceType::Normal) {
                return "○";
            } else {
                return "◎";
            }
        }
    }
}


class Move {
    public $origin;
    public $target;
    public $takes;

    function __construct($origin, $target, $takes = null) {
        $this->origin = $origin;
        $this->target = $target;
        $this->takes = $takes;
    }
}


class Board {
    private $b = [];
    public $size = 8;
    private $rows_with_pieces = 3;

    function __construct() {
        for ($i=0; $i<$this->size; $i++) {
            array_push($this->b, array_fill(0, $this->size, null));
        }
        for ($i=0; $i<$this->rows_with_pieces; $i++) {
            for ($j = $i%2; $j<$this->size; $j += 2) {
                $this->b[$i][$j] = new Piece(Team::Black);
            }
        }
        for ($i=$this->size-$this->rows_with_pieces; $i<$this->size; $i++) {
            for ($j = $i%2; $j<$this->size; $j += 2) {
                $this->b[$i][$j] = new Piece(Team::White);
            }
        }
    }

    function draw_letters() {
        echo " ";
        for ($i=0; $i<$this->size; $i++) {
            echo "  " . ALPHABET[$i] . " ";
        }
        echo "  ";
    }

    function draw_line($left_corner, $line, $intersection, $right_corner) {
        echo " " . $left_corner;
        for ($i=0; $i<$this->size-1; $i++)
            echo $line . $line . $line . $intersection;
        echo $line . $line. $line . $right_corner . " ";
    }

    function draw($cowmsg) {
        $cowlines = cowsay($cowmsg);
        $linenum = 0;
        $this->draw_letters();
        draw_cow_line($cowlines, $linenum);
        echo PHP_EOL;
        $linenum++;
        $this->draw_line("┌", "─", "┬", "┐");
        draw_cow_line($cowlines, $linenum);
        echo PHP_EOL;
        $linenum++;
        foreach ($this->b as $idx=>$row) {
            echo $idx+1;
            foreach ($row as $tile) {
                echo "│";
                if ($tile == null) {
                    echo "   ";
                    continue;
                } else {
                    echo " " . $tile . " ";
                }
            }

            echo "│";
            echo $idx+1;
            draw_cow_line($cowlines, $linenum);
            echo PHP_EOL;
            $linenum++;

            if ($idx != $this->size-1) {
                $this->draw_line("├", "─", "┼", "┤");
                draw_cow_line($cowlines, $linenum);
                echo PHP_EOL;
                $linenum++;
            }
        }
        $this->draw_line("└", "─", "┴", "┘");
        draw_cow_line($cowlines, $linenum);
        echo PHP_EOL;
        $linenum++;
        $this->draw_letters();
        draw_cow_line($cowlines, $linenum);
        echo PHP_EOL;
        $linenum++;
    }

    function get($pos) {
        return $this->b[$pos->x][$pos->y];
    }

    function set($pos, $piece) {
        return $this->b[$pos->x][$pos->y] = $piece;
    }

    function getLegalJumps($pos) {
        $piece = $this->get($pos);
        $team = $piece->team;
        $opponent = ($piece->team + 1) % 2;
        $type = $piece->type;
        $directions = [new Pos(-1, -1), new Pos(-1, 1), new Pos(1, -1), new Pos(1, 1)];

        if ($team == Team::White) {
            $forward = -1;
        } else {
            $forward = 1;
        }

        $legalmoves = [];
        if ($type == PieceType::King) {
            foreach ($directions as $dir) {
                $new_pos = $pos->add($dir);
                while ($new_pos->inBounds($this->size)) {
                    $new_tile = $this->get($new_pos);
                    if ($new_tile != null) {
                        $behind_new_pos = $new_pos->add($dir);
                        if (!$behind_new_pos->inBounds())
                            break;
                        if ($new_tile->team == $opponent && $this->get($behind_new_pos) == null) {
                            $legalmoves[$behind_new_pos->toCoordinates()] = new Move($pos, $behind_new_pos, $new_pos);
                        }
                        break;
                    }
                    $new_pos = $new_pos->add($dir);
                }
            }
        } else {
            foreach ($directions as $dir) {
                $one_pos = $pos->add($dir);
                if (!$one_pos->inBounds($this->size)) {
                    continue;
                }

                $one_tile = $this->get($one_pos);

                $two_pos = $one_pos->add($dir);
                if (!$two_pos->inBounds($this->size)) {
                    continue;
                }

                $two_tile = $this->get($two_pos);
                if ($one_tile != null && $one_tile->team == $opponent && $two_tile == null) {
                    $legalmoves[$two_pos->toCoordinates()] = new Move($pos, $two_pos, $one_pos);
                }
            }
        }
        return $legalmoves;
    }

    // Returns an array of possible moves (non-jumps) from a position
    function getLegalMoves($pos) {
        $piece = $this->get($pos);
        $team = $piece->team;
        $type = $piece->type;
        $directions = [new Pos(-1, -1), new Pos(-1, 1), new Pos(1, -1), new Pos(1, 1)];

        if ($team == Team::White) {
            $forward = -1;
        } else {
            $forward = 1;
        }

        $legalmoves = [];
        if ($type == PieceType::King) {
            foreach ($directions as $dir) {
                $new_pos = $pos->add($dir);
                while ($new_pos->inBounds($this->size)) {
                    $new_tile = $this->get($new_pos);
                    if ($new_tile == null) {
                        $legalmoves[$new_pos->toCoordinates()] = new Move($pos, $new_pos);
                    } else {
                        break;
                    }
                    $new_pos = $new_pos->add($dir);
                }
            }
        } else {
            foreach ($directions as $dir) {
                $one_pos = $pos->add($dir);
                if (!$one_pos->inBounds($this->size)) {
                    continue;
                }

                $one_tile = $this->get($one_pos);
                if ($one_tile == null && $dir->x == $forward) {
                    $legalmoves[$one_pos->toCoordinates()] = new Move($pos, $one_pos);
                    continue;
                }
            }
        }
        return $legalmoves;
    }

    // Applies a move to the board
    function processMove($move) {
        $piece = $this->get($move->origin);
        $this->set($move->origin, null);
        if ($move->takes != null) {
            $this->set($move->takes, null);
        }
        if ($piece->team == Team::White && $move->target->x == 0) {
            $piece->type = PieceType::King;
        }
        if ($piece->team == Team::Black && $move->target->x == $this->size) {
            $piece->type = PieceType::King;
        }
        $this->set($move->target, $piece);
    }

    // Returns an array of positions of pieces that can jump.
    function canJump($team){
        $jump_positions = [];
        for ($i=0; $i<$this->size; $i++) {
            for ($j=0; $j<$this->size; $j++) {
                $pos = new Pos($i, $j);
                $piece = $this->get($pos);
                if ($piece != null && $piece->team == $team && !empty($this->getLegalJumps($pos)))
                    array_push($jump_positions, $pos);
            }
        }
        return $jump_positions;
    }
}


class Game {
    public $turn;
    public $board;

    // Used to keep track of the score and determine the winner.
    public $white_pieces;
    public $black_pieces;

    function __construct() {
        $this->turn = Team::White;
        $this->board = new Board();
        $this->white_pieces = 12;
        $this->black_pieces = 12;
    }

    function switch_turn() {
        // 0 -> 1 and 1 -> 0
        $this->turn = ($this->turn + 1) % 2;
    }

    function processMove($move) {
        if ($move->takes != null) {
            if ($this->turn == Team::Black) {
                $this->white_pieces--;
            } else {
                $this->black_pieces--;
            }
        }
        $this->board->processMove($move);
    }
}


function checkCoordinate($c, $name, $board_size) {
    echo stristr($c[0], substr(ALPHABET, 0, $board_size));
    if (strlen($c) != 2) {
        return "The ${name} coordinate must be specified with 2 characters (like \"D3\").";
    } else if (!ctype_alpha($c[0])) {
        return "First character of the ${name} coordinate must be a letter.";
    } else if (!ctype_digit($c[1])) {
        return "Second character of the ${name} coordinate must be a number";
    } else if (strpos($c[0], ALPHABET) > $board_size) {
        return "First character of the ${name} coordinate is out of bounds.";
    } else if (intval($c[1]) > $board_size) {
        return "Second character of the first coordinate is out of bounds.";
    } else {
        return false;
    }
}


function suggestTargets($moves) {
    $msg = "Try one of the following coordinates:" . PHP_EOL;
    foreach ($moves as $coordinate=>$move) {
        $msg .= $coordinate . " ";
    }
    return $msg;
}


function suggestPieces($can_jump) {
    $msg = "Try one of the following coordinates:" . PHP_EOL;
    foreach ($can_jump as $position) {
        $msg .= $position->toCoordinates() . " ";
    }
    return $msg;
}


function cowReaction($origin, $target, $team, $piece_has_been_taken, $difference) {
    $cowmsg = "${origin} to ${target}." . PHP_EOL;
    if ($piece_has_been_taken) {
        if ($difference == 1) {
            if ($team == Team::Black) {
                $responses = ["BLACK has taken the lead!"];
            } else {
                $responses = ["WHITE has taken the lead!"];
            }
        } else {

            if ($difference > 4) {
                $responses = [
                    "You surely are the Magnus Carlsen of draughts!",
                    "Everybody pay attention! We've got an Einstein here!",
                    "Are you gonna be the new checkers champion?",
                    "Outstanding move!",
                    "Incredible!",
                    "That's exactly what I would've done!",
                    "Putting the opponent in a tough spot!",
                    "Genius!",
                    "Very nice!",
                    "Wow!",
                ];
            } else if ($difference > 0) {
                $responses = [
                    "Outstanding move!",
                    "Incredible!",
                    "That's exactly what I would've done!",
                    "Putting the opponent in a tough spot!",
                    "Genius!",
                    "Very nice!",
                    "Wow!",
                    "Hmmm, interesting move...",
                    "If I actually knew how this game worked\nI'm sure I'd appreciate\nthat move!",
                    "Alright.",
                ];
            } else if ($difference == 0) {
                $responses = [
                    "And the score is even!",
                    "You are very evenly matched!",
                    "What an exciting match!",
                    "The suspense is killing me!"
                ];
            } else if ($difference < 4) {
                $responses = [
                    "OK.",
                    "This is the beginning of a comeback!",
                    "You can still win this!",
                    "Don't give up!",
                    "I believe you can still win!"
                ];
            } else if ($difference < 0) {
                $responses = [
                    "OK.",
                    "Getting closer!",
                    "Very nice!",
                    "Alright.",
                    "That's exactly what I would've done!",
                ];
            }
        }
    } else {
        $responses = [
            "Are you sure that was a good move?",
            "Maybe you ought to reconsider that move...",
            "Let me think about that one.",
            "I wouldn't have done that...",
            "Why would you do that?",
            "Taking it slowly, I see.",
            "Setting up a nice trap?",
            "Ah, I see...",
            "Ah, I see...",
            "Ah, I see...",
            "Moooooo!!",
            "Moooooo!!",
            "Meow, meow, I'm a cow.",
            "Cool, cool, I'm a bull.",
            "...",
            "...",
            "...",
            "...",
            "OK.",
            "OK.",
            "Interesting...",
            "Interesting...",
            "Interesting...",
        ];
    }
    $cowmsg .= $responses[array_rand($responses)];
    return $cowmsg;
}


function main() {
    // The while loop ensures that the game restarts when it ends (after
    // pressing enter).
    while (true) {
        $game = new Game();
        $cowmsg = "Hello there! Here are the rules:
- All movement is diagonal.
- You have to jump when possible.
- Normal pieces can only move forwards.
- But they can jump both forwards and backwards.
- One piece can chain multiple jumps.
- If a man ends his turn on the opposite side, it
  becomes a king.
- Kings can move as far as they want.
- Kings must land directly after the piece they
  capture.";
        while (true) {
            $game->board->draw($cowmsg);
            if ($game->turn == Team::White) {
                echo "It's WHITE's turn.";
            } else {
                echo "It's BLACK's turn.";
            }
            echo PHP_EOL;
            echo "Specify the move in the format: [a-Z][0-9] [a-Z][0-9] (eg \"a5 b4\")," . PHP_EOL;
            echo "where the first coordinate is the origin and the second the target." . PHP_EOL;
            $input = readline('> ');
            $input_array = explode(" ", strtoupper(trim($input)));

            // When we continue, the turn will not be switched and the user can
            // specify a new position.
            if (sizeof($input_array) < 2) {
                $cowmsg = "Not enough coordinates";
                continue;
            }
            if (sizeof($input_array) > 2) {
                $cowmsg = "Too many coordinates";
                continue;
            }

            $origin = $input_array[0];
            $target = $input_array[1];

            $result = checkCoordinate($origin, "first", $game->board->size);
            if ($result) {
                $cowmsg = $result;
                continue;
            }

            $result = checkCoordinate($target, "second", $game->board->size);
            if ($result) {
                $cowmsg = $result;
                continue;
            }

            $origin_pos = Pos::fromCoordinates($origin);
            $origin_piece = $game->board->get($origin_pos);
            if ($origin_piece == null || $origin_piece->team != $game->turn) {
                $cowmsg = "The first coordinate must refer\nto one of your own pieces.";
                continue;
            }

            $can_jump = $game->board->canJump($game->turn);
            if (!empty($can_jump) && !in_array($origin_pos, $can_jump)) {
                $cowmsg = "There are pieces that can jump!" . PHP_EOL;
                $cowmsg .= suggestPieces($can_jump);
                continue;
            }

            $legal_jumps = $game->board->getLegalJumps($origin_pos);
            $legal_moves = $game->board->getLegalMoves($origin_pos);
            if (empty($legal_jumps) && empty($legal_moves)) {
                $cowmsg = "That piece cannot be moved!";
                continue;
            }

            $target_pos = Pos::fromCoordinates($target);
            $target_piece = $game->board->get($target_pos);
            if ($target_piece != null) {
                $cowmsg = "The second coordinate must be an empty tile." . PHP_EOL;
                $cowmsg .= suggestTargets($legal_moves);
                continue;
            }

            if (!empty($legal_jumps)) {
                if (!array_key_exists($target, $legal_jumps)) {
                    $cowmsg = "You have to jump with this piece!" . PHP_EOL;
                    $cowmsg .= suggestTargets($legal_jumps);
                    continue;
                } else {
                    $piece_has_been_taken = true;
                    $game->processMove($legal_jumps[$target]);
                }
            } else {
                if (!array_key_exists($target, $legal_moves)) {
                    $cowmsg = "That's not allowed!" . PHP_EOL;
                    $cowmsg .= suggestTargets($legal_moves);
                    continue;
                } else {
                    $piece_has_been_taken = false;
                    $game->processMove($legal_moves[$target]);
                }
            }

            if ($game->white_pieces == 0) {
                $cowmsg = "BLACK WINS!!!!";
                break;
            }
            if ($game->black_pieces == 0) {
                $cowmsg = "WHITE WINS!!!!";
                break;
            }

            if ($game->turn == Team::White) {
                $difference = $game->white_pieces - $game->black_pieces;
            } else {
                $difference = $game->black_pieces - $game->white_pieces;
            }

            $cowmsg = cowReaction($origin, $target, $game->turn, $piece_has_been_taken, $difference);

            // The code below allows for chaining jumps.
            $origin = $target;
            $origin_pos = $target_pos;
            if ($piece_has_been_taken) {
                while ($game->board->getLegalJumps($origin_pos)) {
                    $game->board->draw($cowmsg);
                    echo "You can jump further with the piece at ${origin}" . PHP_EOL;
                    echo "Enter a new target tile:" . PHP_EOL;
                    $target = strtoupper(trim(readline("> ")));

                    if (sizeof(explode(" ", $target)) > 2) {
                        $cowmsg = "Too many coordinates";
                        continue;
                    }

                    $result = checkCoordinate($target, "first", $game->board->size);
                    if ($result) {
                        $cowmsg = $result;
                        continue;
                    }

                    $legal_jumps = $game->board->getLegalJumps($origin_pos);
                    if (!array_key_exists($target, $legal_jumps)) {
                        $cowmsg = "You have to jump with this piece!" . PHP_EOL;
                        $cowmsg .= suggestTargets($legal_jumps);
                        continue;
                    }

                    $game->processMove($legal_jumps[$target]);

                    if ($game->turn == Team::White) {
                        $difference = $game->white_pieces - $game->black_pieces;
                    } else {
                        $difference = $game->black_pieces - $game->white_pieces;
                    }

                    $cowmsg = cowReaction($origin, $target, $game->turn, true, $difference);

                    $origin = $target;
                    $origin_pos = Pos::fromCoordinates($origin);
                }
            }
            $game->switch_turn();
        }
        $game->board->draw($cowmsg);
        readline("Press Enter to restart...");
    }
}


main();

?>
